#include "line_reader.h"

/*
//Runs over the string and splits the string into elements in the vector by the given character
//This code was inspired from this post: http://stackoverflow.com/questions/5888022/split-string-by-single-spaces
//text: the input text
//strs: the output vector
//del: the character to divide the string with
//Returns the size of the vector being returned
*/
//Function by Abhimanyu Gupta
int split(string txt, vector<string> &strs, string del){
	if(txt.find(" ") != string::npos){ //If the string has spaces
		unsigned int initialPos = 0; //The initial position in the string
		size_t pos = 0; //the current position
		for(pos = txt.find(del); pos != (string::npos); pos = txt.find( del, initialPos )){
			strs.push_back( txt.substr( initialPos, pos - initialPos + 1 ) );
			initialPos = pos + 1;
		}

		strs.push_back( txt.substr( initialPos, min<int>( pos, txt.size()) - initialPos + 1 ) ); //Add the last string to the vector
	} else{ //If the string does not have spaces
		strs.push_back(txt); //Put the string in the vector
	}
	return strs.size(); //Return an integer version of the size of the array
}

/*
//Removes a given character from the input string word
//word: the word to remove the characters from
//del: the character to remove
*/
//Function by Jonathan Leitschuh
string removeChar(string word, string del){
	unsigned int initialPos = 0; //Initial position
	size_t pos = 0;
	for(pos = word.find(del); pos != (string::npos); pos = word.find( del, initialPos )){ //Find all of the 'del' characters in this string
		word.erase(pos); //remove that character
		initialPos = pos + 1; //Increment by one
	}
	return word; //Return the string
}

//Function by Abhimanyu Gupta
//Iterates over the string and removes all the elements in the rmList
void strip(vector<string> &strs, vector<string> &rmList){
	for(std::vector<string>::iterator it = strs.begin(); it != strs.end(); ++it) { //Iterates over the all of the strings
		if(DEBUGLINEREAD) cout << *it << " is being stripped" << endl;
		for(std::vector<string>::iterator ch = rmList.begin(); ch != rmList.end(); ++ch) { //Iterates over all of the characters to remove
			*it = removeChar(*it, *ch); //Remove the character from the that string
		}

		if(*it == ""){ //If the index is simply empty
			it = strs.erase(it); //Remove empty strings from the list
			it--; //Decrement the iterator so that you are in the proper index
		}
	}

}

//Function by Jonathan Leitschuh
//This is the the method to feed the full line into
//Vector containing the the words in the the line you input
void wordsInLine(vector<string> &output, string lineOfWords){

	split( lineOfWords, output, " ");//Splits the string into the vector using the delimiter " "

	const int size = 13; //The size of the remove array
	string remove [size] = {",", "!", ".", "\"", ":", ";", "?", "(", ")", "/", "-", " ", "  "}; //List of characters to remove from the strings
	vector<string> removeList (&remove[0], &remove[0]+ size); //Adds all of the elements of this to the removeList vector
	strip( output, removeList); //Strip the elements in 'remove' from the string
}


