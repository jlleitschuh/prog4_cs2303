
#include "binary_tree.h"
#include "line_reader.h"
#include <stdio.h>
#include <string>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
using namespace std;


/*Function by Abhimanyu Gupta*/
int main(int argc, char **argv){
    BinaryTree *start = NULL;
    if(argc == 2){ //If you pass a paramater to the code
        //Create a bunch of test data
        start = new BinaryTree("Tree", 1);
        BinaryTree *test1 = new BinaryTree("This", 2);
        BinaryTree *test2 = new BinaryTree("this", 2);
        BinaryTree *test3 = new BinaryTree("This", 3);
        BinaryTree *test4 = new BinaryTree("and", 4);
        BinaryTree *test5 = new BinaryTree("This", 5);
        BinaryTree *test6 = new BinaryTree("fine", 2);
        BinaryTree *test7 = new BinaryTree("apple", 2);

        //Insert all of these nodes into the head of the binary tree
        start->insert(test1);
        start->insert(test2);
        start->insert(test3);
        start->insert(test4);
        start->insert(test5);
        start->insert(test6);
        start->insert(test7);
    } else {
        int lineCount = 0;
        string input = "";
        int emptyLines = 0;
        do {
            if(input != ""){ //If the string is not empty
                vector<string> wordList; //Create a new string vector
                wordsInLine(wordList, input); //Take the input line and split each word into a seperate element of the vector input removing exteraneos charaters.

                for(std::vector<string>::iterator it = wordList.begin(); it != wordList.end(); ++it) { //Iterate over the vector and add the elements 
                    BinaryTree *nextWord = new BinaryTree(*it, lineCount); // Create a new binary tree
                    if (start == NULL){
                        start = nextWord; // If the tree is empty then set this word as the base node
                    } else {
                        start->insert(nextWord); //Put this new node into the existing tree
                    }
                }

            } else {
                emptyLines ++;
            }
            getline(cin, input); //Gets the next line
            lineCount ++;
        } while(input.find("EOF") == string::npos); //When the input contains the flag 'EOF' then finish
    }
    
    if(start != NULL){ //If the file does not just contain 'EOF'
        //Prints the header for the list of words
        printf("            Word | Count | Lines\n");
        start->printTree(); 
        delete start;
    } else { //If the file just contains "EOF"
        cout << "The file only contains the 'EOF' flag" << endl;
    }
    
}




