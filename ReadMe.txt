   __                        _    _                       ___        _    _      _      _                                        
   \ \   ___   _ __    __ _ | |_ | |__    __ _  _ __     ( _ )      /_\  | |__  | |__  (_) _ __ ___    __ _  _ __   _   _  _   _ 
    \ \ / _ \ | '_ \  / _` || __|| '_ \  / _` || '_ \    / _ \/\   //_\\ | '_ \ | '_ \ | || '_ ` _ \  / _` || '_ \ | | | || | | |
 /\_/ /| (_) || | | || (_| || |_ | | | || (_| || | | |  | (_>  <  /  _  \| |_) || | | || || | | | | || (_| || | | || |_| || |_| |
 \___/  \___/ |_| |_| \__,_| \__||_| |_| \__,_||_| |_|   \___/\/  \_/ \_/|_.__/ |_| |_||_||_| |_| |_| \__,_||_| |_| \__, | \__,_|
   __         _  _               _             _                     ___                _                           |___/        
  / /    ___ (_)| |_  ___   ___ | |__   _   _ | |__                 / _ \ _   _  _ __  | |_   __ _                                     
 / /    / _ \| || __|/ __| / __|| '_ \ | | | || '_ \               / /_\/| | | || '_ \ | __| / _` |                                    
/ /___ |  __/| || |_ \__ \| (__ | | | || |_| || | | |             / /_\\ | |_| || |_) || |_ | (_| |                                    
\____/  \___||_| \__||___/ \___||_| |_| \__,_||_| |_|             \____/  \__,_|| .__/  \__| \__,_|                                    
                                                                                |_|                                                    

Prog 4

To compile use 'make clean && make'
Compiled using g++

To run use './main <inputFile'
inputFile is the from that holds the data to be processed

To run using built in test data use './main <some arbartary value>'

*NOTE* Originally with the code in main.cpp:

do{
	....
} while (input != "EOF");

When running the program on the CCC Server from a mac machine, the program runs fine. However, when running the program on the CCC Server off a Windows machine, there tends to be a problem. In the main file, the program is unable to escape the do while loop, most likely because of the conditional asking to compare a string to a string. We spoke to the professor, and it isn't apparent as to why there is this discepancy. Nevertheless, it is important to know that the program DOES work, but with the Windows running it, it creates all the nodes in the tree but does not print the table.

FIX:
In order to prevent this problem (assumed to have been created by the strangeness of the windows linecarrage)
while (input != "EOF"); was replaced with while(input.find("EOF") != string::npos);
HOWEVER: This creates the issue that if 'EOF' is found anywhere in the middle of a string it will terminate the program. This should however fix the windows linecarrage issue when inputing a file created on a windows machine.
