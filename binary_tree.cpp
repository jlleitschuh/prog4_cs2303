#include "binary_tree.h"

/*
//Function by Jonathan Leitschuh
//Constructor for a Binary tree node
//Word is the word for this node
//lineNumb is the line number that this word is on
*/
BinaryTree::BinaryTree(string word, int lineNumb){
    for(int i = 0; word[i]; i++){ //Iterate over the input
        nodeWord.push_back(tolower(word[i])); //Make all of the letters in the array to their lower case version
    }
    //If the DEBUGTREE is 1 then print out the node and a line-carriage
    if(DEBUGTREE) cout << nodeWord << " << tree node created" << endl; 
    count = 1; //There is currently one word in this node
    lines.push_back(lineNumb);

    higherChild = NULL; //Initialize the higher child to NULL
    lowerChild = NULL;  //Initialize the lower child to NULL
}

/*
//Function by Abhimanyu Gupta
//Destructor for a Binary tree node
*/
BinaryTree::~BinaryTree(){
    // delete lower child
    if (lowerChild)
        delete lowerChild;
    // delete higher child
    if (higherChild)
        delete higherChild;
}

//Function by Jonathan Leitschuh
//Inserts a binary tree node in its appropriate place on the tree
void BinaryTree::insert(BinaryTree *input){
    if((input->nodeWord) == nodeWord){        //If the words are the same then make the node the same
            count += input-> count; //Add the count of the node being added to the current word count

            vector<int> v;//Create a vector to hold the new vector
            //Find the union of the two vectors (basically preventing repeating numbers) and combines the two to form the new list
            set_union(lines.begin(), lines.end(), input->lines.begin(), input->lines.end(), back_inserter(v));
            lines = v; //replace lines with the new list vector
            delete input;
    }else if((input->nodeWord) > nodeWord){ //If the input word is greater than the word in this node
        if(higherChild == NULL){ //If the higher node is unoccupied
            higherChild = input; //Put the node in the higher child
        } else {                 //Else if the higher node is full
            higherChild->insert(input); //Pass the input into the higher node
        }
    } else {                     //If the input word is less than this nodes word
        if(lowerChild == NULL){  //If the lower child is available
            lowerChild = input;  //Put the input node in the lower child
        } else {                 //if the lower child is unavailable
            lowerChild->insert(input); //Pass the input into the lower node
        }
    }
}

//Function by Jonathan Leitschuh
//Print all of the elements in the binary tree
int BinaryTree::printTree(){
    int wordCount = 0;
    if(lowerChild != NULL){ //If the lower child is occupied
        wordCount += (lowerChild -> printTree()); //Print the elements in that node
    }

    //Print the current node
    printf("%16s   %5d   ", nodeWord.c_str(), count); //Print the word and the count of that word
    for( std::vector<int>::const_iterator i = lines.begin(); i != lines.end(); ++i){ //Iterate over the vector lines
        cout << *i << ", "; //Print out the line number for each element in 'lines'
    }
    cout<< '\b' <<'\b' << ' ' << ' ' << endl; //Backspace two characters in the print statement and replace with empty space (cosmetic cleanup)
    //End print current node

    if(higherChild != NULL){//If the higher child is occupied
        wordCount += (higherChild -> printTree());//Print the elements in that node
    }
    return ++wordCount; //Increment the word count and return this value
}
