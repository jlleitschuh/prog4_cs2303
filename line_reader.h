#ifndef _LINE_READER_H_
#define _LINE_READER_H_

//Include files
#include <string>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iostream>
using namespace std;
//End Include files

//Debug variable: 1 prints debug lines; 0 hides them
#define DEBUGLINEREAD 1

int split(string txt, vector<string> &strs, string del);//Splits the input string using the ' ' delimiter
string removeChar(string word, string del); //Remove the given charater from the string
void strip(vector<string> &strs, vector<string> &rmList);//Remove all of the diferent charaters in the rmList from the strings in strs
void wordsInLine(vector<string> &out, string lineOfWords); //Returns the words as individual elements in the vector when passed a line as a string



#endif
