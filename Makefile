## Start of the Makefile
main: main.o binary_tree.o line_reader.o
	g++ -g -o main main.o binary_tree.o line_reader.o
	
main.o: main.cpp binary_tree.h
	g++ -g -Wall -c main.cpp
	
binary_tree.o: binary_tree.cpp binary_tree.h
	g++ -g -Wall -c binary_tree.cpp

line_reader.o: line_reader.cpp line_reader.h
	g++ -g -Wall -c line_reader.cpp

clean: 
	rm *.o
## End of the Makefile

