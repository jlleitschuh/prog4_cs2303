#ifndef _BINARY_TREE_H_
#define _BINATY_TREE_H_

//Includes
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
#include <iostream>
#include <stdio.h>
using namespace std;
//End include

//Enable to turn on debug print statements.
#define DEBUGTREE 1

//Prototype decleration of the class so that the class can contain itself
class BinaryTree;

class BinaryTree{
    public: //Public variable and mehod declerations
    BinaryTree(string word, int line); //Constructor for the binary tree
    ~BinaryTree(); //Destructor for the binary tree
    void insert(BinaryTree *input); //Inserts the node into the tree given the input node
    int printTree(); //Prints the tree and returns the number of unique words in the tree
    
    private: //Private variable and method decleratins
    std::string nodeWord; //The word for this node 
    int count; // The number of times this word has appeared
    std::vector<int> lines; //The list of lines that this word has appeared

    BinaryTree *higherChild; //Holds the higher child node
    BinaryTree *lowerChild;  //Holds the lower child node 
    
};


#endif
